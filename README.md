# Game of Life  

## Instructions

To launch the server, you can double click on the server.jar file or within the console with the command:  
`java.exe -jar path/to/file/server.jar`

The server runs on the port 9090.  

Once the server is up, you can launch a client ONLY within a console with the command:  
`java.exe -jar path/to/file/client.jar`  

You can add as much clients as you want to the server by running the above command.  

## Libraries

For the settings render, I used the Swing & awt libraries.  

## Miscellaneous

You can see the steps I took during the test through my commits in the link below:  
https://gitlab.com/Laudanomium/GameOfLife/commits/master

Below, you can see what the grid looked like during the development.  

![alt text](png/capt1.PNG)