import java.util.BitSet;
import java.util.Random;

public class GolGrid
{

    private static GolGrid instance;

    private boolean[][] grid;
    private int size;
    private int survivalMax;
    private int survivalMin;
    private double refreshPeriod;
    private Pattern pattern;

    //region Getters
    public int getSize()
    {
        return size;
    }

    public int getSurvivalMax()
    {
        return survivalMax;
    }

    public int getSurvivalMin()
    {
        return survivalMin;
    }

    public Pattern getPattern()
    {
        return pattern;
    }

    public double getRefreshPeriod()
    {
        return refreshPeriod;
    }
    //endregion

    //Constructor used by the builder
    GolGrid(int size, int survivalMin, int survivalMax, double refreshPeriod, Pattern pattern)
    {
        this.size = size;
        this.refreshPeriod = refreshPeriod;

        //Allow wrong sided interval
        if (survivalMin <= survivalMax)
        {
            this.survivalMin = survivalMin;
            this.survivalMax = survivalMax;
        } else
        {
            this.survivalMin = survivalMax;
            this.survivalMax = survivalMin;
        }

        this.pattern = pattern;
        grid = new boolean[size][size];

        if (pattern == Pattern.random)
            initRandom();
        else
            initPattern(pattern);

        instance = this;
    }

    public static synchronized GolGrid getInstance()
    {
        return instance;
    }

    //Fill array with an initial pattern
    private void initPattern(Pattern pattern)
    {
        for (int row = 0; row < size; row++)
        {
            for (int col = 0; col < size; col++)
                grid[row][col] = false;

        }

        switch (pattern)
        {
            case glider:
                grid[size - 1][1] = true; // xxxxxx
                grid[size - 2][2] = true; // xxxxxx
                grid[size - 3][0] = true; // OOOxxx
                grid[size - 3][1] = true; // xxOxxx
                grid[size - 3][2] = true; // xOxxxx
                break;

            default:
                initRandom();
                break;
        }

    }

    //Fill array with random alive cells
    private void initRandom()
    {
        Random random = new Random();
        for (int row = 0; row < size; row++)
        {
            for (int col = 0; col < size; col++)
                grid[row][col] = random.nextBoolean();

        }
    }

    //Get the cells around the current cell
    private boolean[] getNeighbors(int row, int col)
    {
        boolean[] neighbors = new boolean[8];
        int i = 0;

        //Find all surrounding cells by adding +/- 1 to col and row
        for (int colNum = col - 1; colNum <= col + 1; colNum++)
        {
            for (int rowNum = row - 1; rowNum <= row + 1; rowNum++)
            {
                //If not the center cell
                if (!((colNum == col) && (rowNum == row)))
                {
                    //Make sure it is within  grid
                    if (withinGrid(colNum, rowNum))
                    {
                        neighbors[i] = grid[rowNum][colNum];
                    } else
                    {
                        neighbors[i] = false;
                    }
                    i++;
                }
            }
        }
        return neighbors;
    }

    //Check if the cell is within the grid
    private boolean withinGrid(int rowIndex, int colIndex)
    {
        boolean retVal = true;

        if ((colIndex >= size) || (rowIndex >= size) ||
                (colIndex < 0) || (rowIndex < 0))
        {
            retVal = false;
        }

        return retVal;
    }

    //Set the cell state to alive or dead
    private boolean setCellState(boolean[] neighbors, boolean cellState)
    {
        int aliveNeighbors = 0;
        for (boolean b : neighbors)
        {
            if (b)
                aliveNeighbors++;

            //If there are too much alive cells around, the cell dies
            if (aliveNeighbors > survivalMax)
                return false;
        }

        //If there are not enough cells around, the cell dies
        if (aliveNeighbors < survivalMin)
            return false;
            //If there are exactly a number of cells around alive, the cell lives
        else if (aliveNeighbors == survivalMax)
            return true;
            //If there are enough cells around, the cell keeps its state
        else
            return cellState;
    }

    //Reset grid
    public void reset()
    {
        //System.out.println("alo");
        switch (pattern)
        {
            case random:
                initRandom();
                break;

            default:
                initPattern(pattern);
                break;
        }
    }

    //Update grid
    public static void tick()
    {
        GolGrid golGrid = GolGrid.getInstance();
        int size = golGrid.size;

        boolean[] neighbors;
        boolean[][] futureGrid = new boolean[size][size];

        for (int row = 0; row < size; row++)
        {
            for (int col = 0; col < size; col++)
            {
                neighbors = golGrid.getNeighbors(row, col);
                futureGrid[row][col] = golGrid.setCellState(neighbors, golGrid.grid[row][col]);
            }
        }

        golGrid.grid = futureGrid;
    }

    //Render grid in console
    public static void render()
    {
        GolGrid golGrid = GolGrid.getInstance();

        for (boolean[] bCol : golGrid.grid)
        {
            for (boolean b : bCol)
            {
                System.out.print(b ? Statics.ANSI_GREEN + "▓ " : Statics.ANSI_RED + "▓ ");
            }
            System.out.println();
        }
        System.out.println(Statics.ANSI_RESET);
    }

    //Convert grid to string, 1 bool = 1 character
    public static String easyGridToString()
    {
        GolGrid golGrid = GolGrid.getInstance();
        String retVal = "";

        for (boolean[] bCol : golGrid.grid)
        {
            for (boolean b : bCol)
            {
                retVal += (b ? "1" : "0") ;
            }
        }

        return retVal;
    }

    //Convert grid to string, 1 bool = 1 bit
    public static String gridToString()
    {
        GolGrid golGrid = GolGrid.getInstance();
        BitSet bitSet = new BitSet(golGrid.size * golGrid.size);
        int index = 0;

        //Fill bitset with boolean values of the grid
        for (boolean[] bCol : golGrid.grid)
        {
            for (boolean bool : bCol)
            {
                bitSet.set(index, bool);
                index++;
            }
        }
        //Convert bit set sot bytes array
        byte[] bytes = bitSet.toByteArray();
        //Convert bytes array to String with a hashcode
        return bytes.toString();
    }
}

