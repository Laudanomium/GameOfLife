public class GolGridBuilder
{
    private int size = 1;
    private int survivalMax = 3;
    private int survivalMin = 2;
    private double refreshPeriod = 1.0d;
    private Pattern pattern = Pattern.random;

    public GolGridBuilder()
    {
    }

    public GolGrid BuildGolGrid()
    {
        return new GolGrid(size, survivalMin, survivalMax, refreshPeriod, pattern);
    }

    public GolGridBuilder Size(int size)
    {
        this.size = size;
        return this;
    }

    public GolGridBuilder SurvivalMin(int survivalMin)
    {
        this.survivalMin = survivalMin;
        return this;
    }

    public GolGridBuilder SurvivalMax(int survivalMax)
    {
        this.survivalMax = survivalMax;
        return this;
    }

    public GolGridBuilder RefreshPeriod(double refreshPeriod)
    {
        this.refreshPeriod = refreshPeriod;
        return this;
    }

    public GolGridBuilder Pattern(Pattern pattern)
    {
        this.pattern = pattern;
        return this;
    }
}
