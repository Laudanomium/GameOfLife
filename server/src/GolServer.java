import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * A TCP server that runs on port 9090.
 */

public class GolServer
{
    private static final int PORT = 9090;
    private static GolGrid golGrid;
    private static boolean canSetGrid;
    private static int totalClients = 0;
    private static ArrayList<GolThread> golThreads = new ArrayList<>();
    private static ScheduledExecutorService executorService;
    private static Runnable runnable;

    /**
     * Runs the server.
     */
    public static void main(String[] args) throws IOException
    {
        //Initialize variables
        ServerSocket listener = new ServerSocket(PORT);
        canSetGrid = true;

        runnable = new Runnable()
        {
            String outputStr = "";
            @Override
            public void run()
            {
                outputStr = GolGrid.easyGridToString();
                //Go through all the clients and
                //send them the grid update
                for (GolThread thread : golThreads)
                    thread.output.println(outputStr);

                GolGrid.render();
                GolGrid.tick();

            }
        };

        try
        {
            while (true)
            {
                //New connection !
                golThreads.add(new GolThread(listener.accept(), totalClients++));

                //If the client is alone
                if (golThreads.size() == 1)
                {
                    executorService = Executors.newSingleThreadScheduledExecutor();
                    golGrid = GolGrid.getInstance();
                    //Start the game of life using client's settings
                    executorService.scheduleAtFixedRate(runnable, 0,
                            (long)golGrid.getRefreshPeriod(), TimeUnit.SECONDS);
                }
                //If there are two clients or more, the first cannot change the grid anymore
                else if (golThreads.size() == 2)
                {
                    golThreads.get(0).output.println("cannotSetGrid");
                }
                //Start communication with the client
                golThreads.get(totalClients - 1).start();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            //Server shutdown
            listener.close();
        }
    }

    private static class GolThread extends Thread
    {
        private Socket socket;
        private int clientNumber;
        private GolGrid golGrid;
        private BufferedReader input;
        private PrintWriter output;

        public GolThread(Socket socket, int clientNumber)
        {
            this.socket = socket;
            this.clientNumber = clientNumber;

            try
            {
                input = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                output = new PrintWriter(socket.getOutputStream(), true);

                System.out.println("Client #" + clientNumber + " connected");

                //If there is a Game of Life running, send server's grid settings
                if (GolGrid.getInstance() != null)
                {
                    output.println(clientNumber + "|" +
                            GolServer.golGrid.getSize() + "|" +
                            GolServer.golGrid.getSurvivalMin() + "|" +
                            GolServer.golGrid.getSurvivalMax() + "|" +
                            GolServer.golGrid.getRefreshPeriod() + "|" +
                            GolServer.golGrid.getPattern());
                }
                //Else, set the client's number
                else
                {
                    output.println(clientNumber);
                }

                //If the client is alone on the server
                //Wait for client's input to set the game
                while (totalClients == 1 && canSetGrid)
                {
                    String settingsStr = input.readLine();
                    if (settingsStr != null && settingsStr != "")
                    {
                        //parse settings input and build the game of life
                        parseInput(settingsStr);
                        break;
                    }
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        public void run()
        {
            try
            {
                //Listen to specific client's input
                while (true)
                {
                    String in = input.readLine();
                    if (in.contains("reset"))
                    {
                        System.out.println("Reset");
                        GolServer.golGrid.reset();
                    }
                    //Handle client's settings changes if he is allowed to do so
                    else if (in.contains("|") && canSetGrid)
                    {
                        parseInput(in);
                    }
                }
            }
            catch (IOException e)
            {
                //e.printStackTrace();
                System.out.println("Client #"+ clientNumber + " disconnected");
            }
            finally
            {
                //Handle client disconnection
                golThreads.remove(this);
                totalClients--;

                if (totalClients <= 1)
                    canSetGrid = true;

                //Stop grid update if there is no one on the server
                if (totalClients == 0)
                    executorService.shutdown();

                //Allow the last client to modify the grid
                if (golThreads.size() == 1)
                    golThreads.get(0).output.println("canSetGrid");

                try
                {
                    socket.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Logs a simple message.  In this case we just write the
         * message to the server applications standard output.
         */

        private void parseInput(String s)
        {
            //Parse input
            String delim = "[|]";
            String[] settings = s.split(delim);

            int size = Integer.parseInt(settings[0]);
            int survivalMin = Integer.parseInt(settings[1]);
            int survivalMax = Integer.parseInt(settings[2]);
            double refreshPeriod = Double.parseDouble(settings[3]);
            Pattern pattern = Pattern.valueOf(settings[4]);

            //Instantiate the grid
            GolGrid grid = new GolGridBuilder()
                    .Size(size)
                    .SurvivalMin(survivalMin)
                    .SurvivalMax(survivalMax)
                    .RefreshPeriod(refreshPeriod)
                    .Pattern(pattern)
                    .BuildGolGrid();
        }

        private void log(String message) {
            System.out.println(message);
        }
    }

}