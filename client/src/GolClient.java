import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GolClient
{

    //Client-server settings
    private BufferedReader in;
    private PrintWriter out;
    private Socket socket;

    //Grid settings
    private boolean canSetGrid;
    private int clientNumber;
    private int size, survivalMin, survivalMax;
    private double refreshPeriod;
    private Pattern pattern;
    private Runnable runnable;

    //Evolutive render
    private boolean[][] previousGrid;
    private boolean evolutiveRender = false;

    //region Getters Setters
    public BufferedReader getIn()
    {
        return in;
    }

    public void setIn(BufferedReader in)
    {
        this.in = in;
    }

    public PrintWriter getOut()
    {
        return out;
    }

    public void setOut(PrintWriter out)
    {
        this.out = out;
    }

    public Socket getSocket()
    {
        return socket;
    }

    public void setSocket(Socket socket)
    {
        this.socket = socket;
    }

    public boolean isCanSetGrid()
    {
        return canSetGrid;
    }

    public void setCanSetGrid(boolean canSetGrid)
    {
        this.canSetGrid = canSetGrid;
    }

    public int getClientNumber()
    {
        return clientNumber;
    }

    public void setClientNumber(int clientNumber)
    {
        this.clientNumber = clientNumber;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public int getSurvivalMin()
    {
        return survivalMin;
    }

    public void setSurvivalMin(int survivalMin)
    {
        this.survivalMin = survivalMin;
    }

    public int getSurvivalMax()
    {
        return survivalMax;
    }

    public void setSurvivalMax(int survivalMax)
    {
        this.survivalMax = survivalMax;
    }

    public double getRefreshPeriod()
    {
        return refreshPeriod;
    }

    public void setRefreshPeriod(double refreshPeriod)
    {
        this.refreshPeriod = refreshPeriod;
    }

    public Pattern getPattern()
    {
        return pattern;
    }

    public void setPattern(Pattern pattern)
    {
        this.pattern = pattern;
    }

    public boolean isEvolutiveRender()
    {
        return evolutiveRender;
    }

    public void setEvolutiveRender(boolean evolutiveRender)
    {
        this.evolutiveRender = evolutiveRender;
    }

    //endregion

    private void connect() throws IOException
    {
        //Initialize runnable
        runnable = new Runnable()
        {
            public void run()
            {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                InputFrame.createAndShowGUI(GolClient.this);
            }
        };

        //Connect to server first
        String serverAddress = JOptionPane.showInputDialog(
                "Enter IP Address of a machine that is\n" +
                        "running the game on port 9090:");

        socket = new Socket(serverAddress, 9090);

        //Set input/output stream
        in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

        //Handle server's data on connection
        String serverData = in.readLine();
        //Initialize client's grid settings
        initVariables(serverData);

        //If there is none client, the user can set the grid
        canSetGrid = clientNumber == 0;

        //Render settings frame
        SwingUtilities.invokeLater(runnable);
        //Start game loop
        watch();
    }

    private void disconnect() throws IOException
    {
        socket.close();
    }

    //Send the client parsed settings to the server
    public void sendSettings() throws IOException
    {
        if (canSetGrid)
        {
            out.println(parseInput());
        }
    }

    //Request a grid reset from the server
    // !! Allow a reset even when several clients are watching !!
    // !! Can be disabled with a  canSetGrid  check            !!
    public void requestReset() throws IOException
    {
        if (previousGrid != null)
            out.println("reset");
    }

    //Main loop, wait for server's response to render the grid
    private void watch() throws IOException
    {
        GolClient client = this;
        String response = "";
        boolean[][] grid = new boolean[size][size];

        //Wait for server's messages
        while (true)
        {
            response = in.readLine();
            //If the server's grid has been updated
            if (response != "" && response != null)
            {
                //If there is no other client watching,
                //this one can set the server's grid settings
                if (response.contains("canSetGrid"))
                {
                    canSetGrid = true;
                }
                else if (response.contains("cannotSetGrid"))
                {
                    canSetGrid= false;
                }
                //Else, the response contains the grid state
                else
                {
                    //Deserialize string response to render grid
                    grid = easyStringToGrid(response);

                    //If the checkbox Evolutive is checked
                    if (evolutiveRender)
                        evolutionRender(grid);
                    else
                        render(grid);

                    response = "";
                }
            }
        }
    }

    //Render the grid in console, Green=Alive, Red=Dead
    // o = alive, x = dead
    private void render(boolean[][] grid)
    {
        for (boolean[] bCol : grid)
        {
            for (boolean b : bCol)
            {
                if (!Statics.JAR)
                    System.out.print(b ? Statics.ANSI_GREEN + "▓ " : Statics.ANSI_RED + "▓ ");
                else
                    System.out.print(b ? "o " : "x ");
            }

            System.out.println();
        }
        previousGrid = grid;
        System.out.println(Statics.ANSI_RESET);
    }

    //Render the grid by taking into account previous cells states
    //Blue = Alive       , Grey  = Dead
    //Green = Dead->Alive, Red  = Alive->Dead
    //
    //o = Alive      , x = Dead
    //O = Dead->Alive, X = Alive->Dead
    private void evolutionRender(boolean[][] grid)
    {
        if (previousGrid == null)
        {
            previousGrid = grid;
            render(grid);
        }
        else
        {
            boolean prevState;
            boolean currentState;
            for (int row = 0; row< size; row++)
            {
                for (int col= 0; col< size; col++)
                {
                    prevState = previousGrid[row][col];
                    currentState = grid[row][col];

                    if (!Statics.JAR)
                    {
                        //Walker!!!
                        if (prevState != currentState && currentState)
                            System.out.print(Statics.ANSI_GREEN + "▓ ");
                            //Shot in the head
                        else if (prevState != currentState && !currentState)
                            System.out.print(Statics.ANSI_RED + "▓ ");
                            //Survivor
                        else if (prevState == currentState && currentState)
                            System.out.print(Statics.ANSI_BLUE + "▓ ");
                            //Zombies
                        else //if (prevState == currentState && !currentState)
                            System.out.print(Statics.ANSI_WHITE + "▓ ");
                    }
                    else
                    {
                        //Walker!!!
                        if (prevState != currentState && currentState)
                            System.out.print("O");
                            //Shot in the head
                        else if (prevState != currentState && !currentState)
                            System.out.print("X");
                            //Survivor
                        else if (prevState == currentState && currentState)
                            System.out.print("o");
                            //Zombies
                        else //if (prevState == currentState && !currentState)
                            System.out.print("x");
                    }
                }
                System.out.println();
            }
            System.out.println(Statics.ANSI_RESET);
            previousGrid = grid;
        }

    }

    //Convert string to grid, 1 character = 1 bool
    private boolean[][] easyStringToGrid(String s)
    {
        boolean[][] renderGrid = new boolean[size][size];

        for (int col = 0; col < size; col++)
        {
            for (int row = 0; row < size; row++)
            {
                renderGrid[col][row] = s.charAt(row + col * size) == '1';
            }
        }

        return renderGrid;
    }

    //region TODO
    /*
    //NOT FUNCTIONAL
    //Convert string to grid, 1 bit = 1 bool
    //TODO: Fix function
    @Deprecated
    private  boolean[][] stringToGrid(String string)
    {
        GolGrid golGrid = GolGrid.getInstance();
        byte[] bytes = string.getBytes();
        boolean[] bits = new boolean[bytes.length * 8];

        for (int i = 0; i < bytes.length * 8; i++) {
            if ((bytes[i / 8] & (1 << (7 - (i % 8)))) > 0)
                bits[i] = true;
        }
        System.out.println(bytes.length);
        boolean[][] retVal = new boolean[size][size];

        int row = 0;
        int col = 0;

        for (int i = 0; i < bits.length; i++)
        {
            if (row < size && col < size)
            {
                retVal[row][col] = bits[i];
                row++;

                if (row == size)
                {
                    row = 0;
                    col++;
                }
            }
            else
                break;
        }

        return retVal;
    }*/
    //endregion

    //Parse client's settings to send them to the server
    private String parseInput()
    {
        String retVal =  size + "|" +
                survivalMin + "|" +
                survivalMax + "|" +
                refreshPeriod + "|" +
                pattern.toString();
        System.out.println(retVal);
        return retVal;
    }

    //Init client's number and grid settings
    private void initVariables(String s)
    {
        String delim = "[|]";
        String[] params = s.split(delim);
        clientNumber = Integer.parseInt(params[0]);

        //If the grid is already instantiated, get settings
        if (clientNumber > 0)
        {
            size = Integer.parseInt(params[1]);
            survivalMin = Integer.parseInt(params[2]);
            survivalMax = Integer.parseInt(params[3]);
            refreshPeriod = Double.parseDouble(params[4]);
            pattern = Pattern.valueOf(params[5]);
        }
    }

    @Override
    public String toString()
    {
        return getClass() + " | size: " + size
                + " | min: " + survivalMin
                + " | max: " + survivalMax
                + " | refresh: " + refreshPeriod;
    }

    /**
     * Runs the client as an application.
     */
    public static void main(String[] args)
    {
        GolClient client = new GolClient();
        try
        {
            client.connect();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}