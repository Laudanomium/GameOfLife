import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

public class InputFrame extends JPanel
        implements PropertyChangeListener
{

    //References
    private GolClient client;
    private JFrame frame;

    //Values for the fields
    private int size = 8;
    private int survivalMin = 2;
    private int survivalMax = 3;
    private double refreshPeriod = 5.0d; //seconds
    private Pattern pattern = Pattern.random;

    //region Labels, strings, fields
    //Labels to identify the fields
    private JLabel sizeLabel;
    private JLabel survivalMinLabel;
    private JLabel survivalMaxLabel;
    private JLabel refreshPeriodLabel;
    private JLabel patternLabel;

    private JButton submitButton;
    private JButton resetButton;
    private JButton quitButton;
    private JCheckBox evolutionRenderBox;

    //Strings for the labels
    private static String sizeStr = "Board size: ";
    private static String survivalMinStr = "Lower interval: ";
    private static String survivalMaxStr = "Upper interval: ";
    private static String refreshPeriodStr = "Refresh period: ";
    private static String patternStr = "Pattern: ";

    private static String submitStr = "Go!";
    private static String resetStr = "Reset";
    private static String quitStr = "Quit";
    private static String evolutiveRenderStr = "Evolutive";

    //Fields for data entry
    private JFormattedTextField sizeField;
    private JFormattedTextField survivalMinField;
    private JFormattedTextField survivalMaxField;
    private JFormattedTextField refreshPeriodField;
    private JComboBox<Pattern> patternBox;
    //endregion

    public InputFrame(JFrame frame, GolClient client)
    {
        super(new BorderLayout());

        this.frame = frame;
        this.client = client;

        //Create the labels.
        sizeLabel = new JLabel(sizeStr);
        survivalMinLabel = new JLabel(survivalMinStr);
        survivalMaxLabel = new JLabel(survivalMaxStr);
        refreshPeriodLabel = new JLabel(refreshPeriodStr);
        patternLabel = new JLabel(patternStr);

        //Submit settings
        submitButton = new JButton(submitStr);
        //Handle onClick
        submitButton.addActionListener((ActionEvent e) ->
        {
            //If inputs are correct, set client & server grid settings
            if (size > 0 &&
                survivalMin >= 0 &&
                survivalMax > 0 &&
                refreshPeriod > 0 &&
                client.isCanSetGrid())
            {
                client.setSize(size);
                client.setSurvivalMin(survivalMin);
                client.setSurvivalMax(survivalMax);
                client.setRefreshPeriod(refreshPeriod);
                client.setPattern(pattern);

                //frame.dispose();

                //Temporary fix
                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            client.sendSettings();
                        }
                        catch (IOException ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                }).start();
            }
            //Else show incorrect fields to the user
            else
            {
                //Error feedback
                if (size < 0)
                    patternLabel.setForeground(Color.RED);
                if (survivalMin < 0)
                    survivalMinLabel.setForeground(Color.RED);
                if (survivalMax <= 0)
                    survivalMaxLabel.setForeground(Color.RED);
                if (refreshPeriod <= 0)
                    refreshPeriodLabel.setForeground(Color.RED);
                if (!client.isCanSetGrid())
                    JOptionPane.showMessageDialog(
                            this,
                            "The game is already running",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
            }
        });

        //Reset the grid
        resetButton = new JButton(resetStr);
        resetButton.addActionListener(e ->
        {
            try
            {
                client.requestReset();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        });

        //Disconnect from the server and close application
        quitButton = new JButton(quitStr);
        quitButton.addActionListener(e ->
        {
            System.exit(0);
        });

        //Enable evolutive render
        evolutionRenderBox = new JCheckBox(evolutiveRenderStr);
        evolutionRenderBox.addActionListener(e ->
        {
            client.setEvolutiveRender(!client.isEvolutiveRender());
        });

        //region Text fields
        //Create the text fields and set them up.
        //Size, survival min, survival max, period refresh, pattern
        sizeField = new JFormattedTextField();
        sizeField.setValue(new Integer(size));
        sizeField.setColumns(10);
        sizeField.addPropertyChangeListener("value", this);

        survivalMinField = new JFormattedTextField();
        survivalMinField.setValue(new Integer(survivalMin));
        survivalMinField.setColumns(10);
        survivalMinField.addPropertyChangeListener("value", this);

        survivalMaxField = new JFormattedTextField();
        survivalMaxField.setValue(new Integer(survivalMax));
        survivalMaxField.setColumns(10);
        survivalMaxField.addPropertyChangeListener("value", this);

        refreshPeriodField = new JFormattedTextField();
        refreshPeriodField.setValue(new Float(refreshPeriod));
        refreshPeriodField.setColumns(10);
        refreshPeriodField.addPropertyChangeListener("value", this);

        patternBox = new JComboBox<>(Pattern.values());
        patternBox.setSelectedIndex(1);
        patternBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                JComboBox cb = (JComboBox) e.getSource();
                pattern = Pattern.valueOf(cb.getSelectedItem().toString());
            }
        });


        //endregion

        //Tell accessibility tools about label/textfield pairs.
        sizeLabel.setLabelFor(sizeField);
        survivalMinLabel.setLabelFor(survivalMinField);
        survivalMaxLabel.setLabelFor(survivalMaxField);
        refreshPeriodLabel.setLabelFor(refreshPeriodField);

        //region Fancy layouts
        //Lay out the labels in a panel.
        JPanel labelPane = new JPanel(new GridLayout(0, 1));
        labelPane.add(sizeLabel);
        labelPane.add(survivalMinLabel);
        labelPane.add(survivalMaxLabel);
        labelPane.add(refreshPeriodLabel);
        labelPane.add(patternLabel);
        labelPane.add(evolutionRenderBox);
        labelPane.add(resetButton);

        //Layout the text fields in a panel.
        JPanel fieldPane = new JPanel(new GridLayout(0, 1));
        fieldPane.add(sizeField);
        fieldPane.add(survivalMinField);
        fieldPane.add(survivalMaxField);
        fieldPane.add(refreshPeriodField);
        fieldPane.add(patternBox);
        fieldPane.add(submitButton);
        fieldPane.add(quitButton);

        //Put the panels in this panel, labels on left,
        //text fields on right.
        setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        add(labelPane, BorderLayout.CENTER);
        add(fieldPane, BorderLayout.LINE_END);
        //endregion
    }

    /**
     * Called when a field's "value" property changes.
     */
    public void propertyChange(PropertyChangeEvent e)
    {
        Object source = e.getSource();
        if (source == sizeField)
        {
            size = ((Number) sizeField.getValue()).intValue();
        } else if (source == survivalMinField)
        {
            survivalMin = ((Number) survivalMinField.getValue()).intValue();
        } else if (source == survivalMaxField)
        {
            survivalMax = ((Number) survivalMaxField.getValue()).intValue();
        } else if (source == refreshPeriodField)
        {
            refreshPeriod = ((Number) refreshPeriodField.getValue()).doubleValue();
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    public static void createAndShowGUI(GolClient client)
    {
        //Create and set up the window.
        JFrame frame = new JFrame("Game of Life");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add contents to the window.
        frame.add(new InputFrame(frame, client));

        //Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setSize(frame.getWidth() + 30, frame.getHeight() + 30);
        frame.setVisible(true);
    }
}